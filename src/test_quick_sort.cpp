#include <iostream>
#include <random>
#include <sstream>
#include <algorithm>
#include <vector>
#include <chrono>
#include <ctime>
#include <cmath>
#include "../include/sorting-algorithms.h"

using namespace sa;

int main( void )
{
    std::random_device rd;
    std::mt19937 g(rd());
    srand(time(NULL));
    size_t max = (size_t) pow(10, 5);
    size_t min = (size_t) pow(10, 3);
    size_t inc = (max - min)/25;
    unsigned int * array = new unsigned int[max];
    std::chrono::time_point<std::chrono::steady_clock> start;
    std::chrono::time_point<std::chrono::steady_clock> end;
    std::vector<std::chrono::duration<double, std::milli>> result;
    std::ostringstream cols;

    //colunas
    cols << "r\\c";
    for (size_t len{min}; len<=max; len+=inc){
        cols << "," << len;
    }
    std::cout<<cols.str()<<std::endl;


    // Caso 1: arranjos com elementos em ordem nao decrescente.

    for (size_t len{min}; len<=max; len+=inc){
        for (size_t i{0}; i<len; ++i){
            if (i%10==0)
                array[i] = i;
            else
                array[i] = array[i-1];        
        }

        start = std::chrono::steady_clock::now();
        
        quick_sort(array, array+len);
       
        end = std::chrono::steady_clock::now();
        
        result.push_back(end - start);
    }

    cols = std::ostringstream();
    cols << "c1";
    for (size_t i{0}; i<result.size(); ++i){
        cols << "," <<  result[i].count();
    }
    std::cout<<cols.str()<<std::endl;

    // Caso 2: arranjos com elemento em ordem nao crescente.
    result.clear();
    for (size_t len{min}; len<=max; len+=inc){
        for (size_t i{0}; i<len; ++i){
            if (i%10==0)
                array[i] = max - i;
            else
                array[i] = max - array[i-1];        
        }

        start = std::chrono::steady_clock::now();
        
        quick_sort(array, array+len);
       
        end = std::chrono::steady_clock::now();
        
        result.push_back(end - start);
    }

    cols = std::ostringstream();
    cols << "c2";
    for (size_t i{0}; i<result.size(); ++i){
        cols << "," <<  result[i].count();
    }
    std::cout<<cols.str()<<std::endl;

    // Caso 3: arranjos com elementos 100% aleat´orios.
    result.clear();
    for (size_t len{min}; len<=max; len+=inc){
        for (size_t i{0}; i<len; ++i){
            array[i] = rand() % max;
        }

        start = std::chrono::steady_clock::now();
        
        quick_sort(array, array+len);
       
        end = std::chrono::steady_clock::now();
        
        result.push_back(end - start);
    }

    cols = std::ostringstream();
    cols << "c3";
    for (size_t i{0}; i<result.size(); ++i){
        cols << "," <<  result[i].count();
    }
    std::cout<<cols.str()<<std::endl;
    
    // Caso 4: arranjos com 75% de seus elementos em sua posi¸c˜ao definitiva.
    result.clear();
    for (size_t len{min}; len<=max; len+=inc){
        std::shuffle(array+((int) (len*0.75)), array+len, g);

        start = std::chrono::steady_clock::now();
        
        quick_sort(array, array+len);
       
        end = std::chrono::steady_clock::now();
        
        result.push_back(end - start);
    }

    cols = std::ostringstream();
    cols << "c4";
    for (size_t i{0}; i<result.size(); ++i){
        cols << "," <<  result[i].count();
    }
    std::cout<<cols.str()<<std::endl;

    // Caso 5: arranjos com 50% de seus elementos em sua posi¸c˜ao definitiva.
    result.clear();
    for (size_t len{min}; len<=max; len+=inc){
        std::shuffle(array+((int) (len*0.5)), array+len, g);

        start = std::chrono::steady_clock::now();
        
        quick_sort(array, array+len);
       
        end = std::chrono::steady_clock::now();
        
        result.push_back(end - start);
    }

    cols = std::ostringstream();
    cols << "c5";
    for (size_t i{0}; i<result.size(); ++i){
        cols << "," <<  result[i].count();
    }
    std::cout<<cols.str()<<std::endl;

    // Caso 6: arranjos com 25% de seus elementos em sua posi¸c˜ao definitiva.
    result.clear();
    for (size_t len{min}; len<=max; len+=inc){
        std::shuffle(array+((int) (len*0.25)), array+len, g);

        start = std::chrono::steady_clock::now();
        
        quick_sort(array, array+len);
       
        end = std::chrono::steady_clock::now();
        
        result.push_back(end - start);
    }

    cols = std::ostringstream();
    cols << "c6";
    for (size_t i{0}; i<result.size(); ++i){
        cols << "," <<  result[i].count();
    }
    std::cout<<cols.str()<<std::endl;

    delete []array;
    return EXIT_SUCCESS;
}